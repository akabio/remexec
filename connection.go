package remexec

import "io"

// Connection is a remote client where one can run bash scripts
// mainly for ssh used but for test mocks or possibly container API
// other implementations can be built
type Connection interface {
	Run(cmd string) (string, string, error)
	RunAs(user string, cmd string) (string, string, error)

	RunWithStdin(cmd string, in io.Reader) (string, string, error)
	UploadFile(path string, user string, group string, mod string, r io.Reader) (string, error)
}
