package ssh

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"golang.org/x/crypto/ssh"
)

func makeHost(s Server) string {
	if s.Port == 0 {
		return s.Host + ":22"
	}

	return s.Host + ":" + strconv.Itoa(s.Port)
}

func makeConfig(server Server) (*ssh.ClientConfig, error) {
	authMethod := []ssh.AuthMethod{}

	signer, err := interpreteKey(server.Key)
	if err != nil {
		return nil, err
	}

	authMethod = append(authMethod, ssh.PublicKeys(signer))

	return &ssh.ClientConfig{
		User:              server.User,
		Auth:              authMethod,
		HostKeyCallback:   hostKeyFingerprint(server.Fingerprints),
		Timeout:           time.Second * 10,
		HostKeyAlgorithms: server.HostKeyAlgorithms,
	}, nil
}

func interpreteKey(key string) (ssh.Signer, error) {
	if isCertificate(key) {
		return fromSource(key)
	}

	return fromFile(key)
}

func fromSource(key string) (ssh.Signer, error) {
	return ssh.ParsePrivateKey([]byte(key))
}

func fromFile(key string) (ssh.Signer, error) {
	d, err := os.ReadFile(key)
	if err != nil {
		return nil, fmt.Errorf("could not read key from file %v, %w", key, err)
	}

	return fromSource(string(d))
}

func isCertificate(key string) bool {
	return strings.Contains(key, "-----BEGIN")
}
