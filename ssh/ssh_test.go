package ssh_test

import (
	"bytes"
	"testing"

	"gitlab.com/akabio/expect"
	"gitlab.com/akabio/remexec"
	"gitlab.com/akabio/remexec/ssh"
)

const fingerprint = "SHA256:6B2kcvW7H7Nf1mwG0daNiVCN8AiPAiHypCx95JPWXlM"

func TestConnection(t *testing.T) {
	mkCon(t)
}

func TestManyCommands(t *testing.T) {
	c := mkCon(t)
	for i := 0; i < 100; i++ {
		o, _, _ := c.Run("echo hamster")
		expect.Value(t, "output", o).ToBe("hamster\n")
	}
}

func TestStdout(t *testing.T) {
	con := mkCon(t)

	o, e, _ := con.Run("echo '1234'")

	expect.Value(t, "stdout", o).ToBe("1234\n")
	expect.Value(t, "stderr", e).ToBe("")
}

func TestStderr(t *testing.T) {
	con := mkCon(t)

	o, e, _ := con.Run("cat --x")

	expect.Value(t, "stdout", o).ToBe("")
	expect.Value(t, "stderr", e).ToBe(`cat: unrecognized option '--x'
Try 'cat --help' for more information.
`)
}

func TestStdin(t *testing.T) {
	con := mkCon(t)

	o, e, err := con.RunWithStdin("cat /dev/stdin", bytes.NewReader([]byte("AlohA!")))

	expect.Error(t, err).ToBe(nil)
	expect.Value(t, "stdout", o).ToBe("AlohA!")
	expect.Value(t, "stderr", e).ToBe("")
}

func TestCmd(t *testing.T) {
	o, _, _ := mkCon(t).Run("echo foo")

	expect.Value(t, "stdout", o).ToBe("foo\n")
}

func TestManyCmd(t *testing.T) {
	o, _, _ := mkCon(t).Run("echo foo\necho bar")

	expect.Value(t, "stdout", o).ToBe("foo\nbar\n")
}

func TestCmdAsUser(t *testing.T) {
	o, e, err := mkCon(t).RunAs("ubuntu", "echo foo > ~/test.txt\nls -l ~/test.txt")

	expect.Value(t, "stdout", o).ToHavePrefix("-rw-r--r-- 1 ubuntu root")
	expect.Value(t, "stderr", e).ToBe("")
	expect.Error(t, err).ToBe(nil)
}

func TestCopyFile(t *testing.T) {
	con := mkCon(t)

	_, err := con.UploadFile("/testfile.txt", "ubuntu", "root", "600", bytes.NewReader([]byte("AlohA!")))
	expect.Error(t, err).ToBe(nil)

	o, _, _ := con.Run("ls -l /testfile.txt")
	expect.Value(t, "stdout", o).ToHavePrefix("-rw------- 1 ubuntu root")
}

func TestCopyFileWithSpaceAndHyphens(t *testing.T) {
	con := mkCon(t)

	_, err := con.UploadFile(`/tes t"fi'l'e.t\"xt`, "ubuntu", "root", "600", bytes.NewReader([]byte("AlohA!")))
	expect.Error(t, err).ToBe(nil)

	o, e, _ := con.Run(`ls -l "/tes t\"fi'l'e.t\\\"xt"`)
	expect.Value(t, "stdout", o).ToHavePrefix("-rw------- 1 ubuntu root")
	expect.Value(t, "stderr", e).ToBe("")
}

func mkCon(t *testing.T) remexec.Connection {
	t.Helper()

	con, err := ssh.New(ssh.Server{
		Host:         "localhost",
		Port:         2200,
		User:         "root",
		Key:          "../testsetup/sshserver/testkey",
		Fingerprints: []string{fingerprint},
	})
	if err != nil {
		t.Fatal(err)
	}

	return con
}
