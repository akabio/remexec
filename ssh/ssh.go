package ssh

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net"
	"path/filepath"
	"strings"

	"golang.org/x/crypto/ssh"
)

type Server struct {
	Host              string
	Port              int
	User              string
	Key               string
	Fingerprints      []string
	HostKeyAlgorithms []string
}

// Connection is an ssh remote. handles auth and collection of logs.
type Connection struct {
	client  *ssh.Client
	clients []*ssh.Client
}

// New creates a new Connection to the last server parameter using the other
// servers as jump hosts.
func New(servers ...Server) (*Connection, error) {
	if len(servers) == 0 {
		return nil, errors.New("must provide at least one server to connect to")
	}

	first := servers[0]

	fConfig, err := makeConfig(first)
	if err != nil {
		return nil, err
	}

	clients := []*ssh.Client{}

	client, err := ssh.Dial("tcp", makeHost(first), fConfig)
	if err != nil {
		return nil, err
	}

	clients = append(clients, client)

	for _, server := range servers[1:] {
		con, err := client.Dial("tcp", makeHost(server))
		if err != nil {
			return nil, err
		}

		config, err := makeConfig(server)
		if err != nil {
			return nil, err
		}

		client, err = dialConn(con, makeHost(server), config)
		if err != nil {
			return nil, err
		}

		clients = append(clients, client)
	}

	return &Connection{
		client:  client,
		clients: clients,
	}, nil
}

func dialConn(conn net.Conn, addr string, config *ssh.ClientConfig) (*ssh.Client, error) {
	c, chans, reqs, err := ssh.NewClientConn(conn, addr, config)
	if err != nil {
		return nil, err
	}

	return ssh.NewClient(c, chans, reqs), nil
}

func (c *Connection) Run(cmd string) (string, string, error) {
	return c.run(cmd, nil)
}

func (c *Connection) RunWithStdin(cmd string, in io.Reader) (string, string, error) {
	return c.run(cmd, in)
}

func (c *Connection) RunAs(user string, cmd string) (string, string, error) {
	s := fmt.Sprintf(`sudo su %v << 'HDTpYgd7Dhd34'
%v

HDTpYgd7Dhd34
`, user, cmd)

	return c.run(s, nil)
}

func (c *Connection) UploadFile(path string, user string, group string, mod string, r io.Reader) (string, error) {
	p, f := filepath.Split(path)
	tmpPath := filepath.Join(p, "."+f+".remexec")

	path = escape(path)
	tmpPath = escape(tmpPath)

	cmd := fmt.Sprintf(`sudo tee %v > /dev/null
sudo mv %v %v
sudo chown %v:%v %v
sudo chmod %v %v
`, tmpPath, tmpPath, path, user, group, path, mod, path)

	fmt.Println(cmd)

	_, e, err := c.run(cmd, r)

	return e, err
}

func (c *Connection) run(src string, reader io.Reader) (string, string, error) {
	session, err := c.client.NewSession()
	if err != nil {
		return "", "", err
	}
	defer session.Close()

	var stdoutBuf bytes.Buffer
	session.Stdout = &stdoutBuf

	var stderrBuf bytes.Buffer
	session.Stderr = &stderrBuf

	session.Stdin = reader

	err = session.Run(src)

	result := stdoutBuf.String()
	stderr := stderrBuf.String()

	return result, stderr, err
}

// Closes the connection
func (c *Connection) Close() {
	c.client.Close()
}

func escape(i string) string {
	i = strings.ReplaceAll(i, `\`, `\\`)
	i = strings.ReplaceAll(i, `"`, `\"`)

	return `"` + i + `"`
}
