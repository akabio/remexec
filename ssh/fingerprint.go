package ssh

import (
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"net"
	"strings"

	"golang.org/x/crypto/ssh"
)

// hostKeyFingerprint returns a hostKeyCallback that checks the host key against the given
// fingerprint. Currently only `SHA256:DATA` fingerprints are allowed where data is base64
// encoded with trailing `=` chars removed.
// The SHA256: part is optional.
func hostKeyFingerprint(fps []string) ssh.HostKeyCallback {
	fingerprints := map[string]bool{}

	for _, fp := range fps {
		fp = strings.TrimSpace(fp)
		fp = strings.TrimPrefix(fp, "SHA256:")
		fingerprints[fp] = true
	}

	return func(hostname string, remote net.Addr, key ssh.PublicKey) error {
		hostFingerprint := calculateSHA256Fingerprint(key.Marshal())

		if !fingerprints[hostFingerprint] {
			return fmt.Errorf("fingerprint mismatch %v != %v", hostFingerprint, fps)
		}

		return nil
	}
}

func calculateSHA256Fingerprint(bytes []byte) string {
	hash := sha256.New()
	hash.Write(bytes) // Write never returns an error
	hashBytes := hash.Sum([]byte{})

	return base64.RawStdEncoding.EncodeToString(hashBytes)
}
