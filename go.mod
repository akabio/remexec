module gitlab.com/akabio/remexec

go 1.18

require (
	gitlab.com/akabio/expect v0.9.7
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	golang.org/x/sys v0.0.0-20220627191245-f75cf1eec38b // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
