remexec
=======

A shell exec client, intended for automated execution of commands on remote machines.

Only a small set of operations is available.

ssh implementation
------------------

Supports jumphosts.

The target system needs following commands:
  - sudo
  - su
  - tee
  - chmod
  - chown
  - mv

The user of the connection must be in the sudoers file with no password required.